#constants
path = '/var/www/Text.txt'
width = 232
height = 16
triggers = 0

fps = 7

cooldown = 2

prevJsonData = ""
import json
import pyinotify
import time
import RPi.GPIO as GPIO

GPIO.setwarnings(False) #gpio init
GPIO.setmode(GPIO.BOARD)
GPIO.setup(12, GPIO.OUT)
GPIO.output(12, GPIO.HIGH)


from bibliopixel.led import *
from bibliopixel.animation import *
from bibliopixel.matrix_animations import *
from bibliopixel.strip_animations import *
import bibliopixel.image as image

prepTX()
#bibliopixel init
from bibliopixel.drivers.serial_driver2 import DriverSerial 
driver = DriverSerial(type = 0, num = width*height, dev = "/dev/ttyAMA0", baud = 1152000, SPISpeed = 12, gamma = range(256))

led = LEDMatrix(driver, height, width, rotation = MatrixRotation.ROTATE_90, serpentine = False)

#prepare Teensy for data transfer
def prepTX():
	GPIO.output(12, GPIO.LOW)
	GPIO.output(12, GPIO.HIGH)

#push frame to teensy
def updateLeds():
	print("Updating Display... ")
	prepTX()
	led.update()
	print "done"

#gets called, when Text.txt changes
def updateDisplay():
	led.all_off()
	f = open(path,'r')
	jsonData = f.read()
	if jsonData != "":
		prevJsonData=jsonData
		print jsonData
		data = json.loads(jsonData)
		
		if data["type"] == "text": #show text
			
			if data["line1"]["scrolling"] == "on": #show scrolling text
				anim = ScrollText(led, data["line1"]["text"], width, 4, color = eval(data["line1"]["color"]))
				anim.run(fps = fps)
			else: #show static text
				for i in range(1,int(data["lines"])+1): #	  <							  calculate middle of text								   >
					led.drawText(data["line"+str(i)]["text"], int((width*float(data["position"]))-(6*int(data["line"+str(i)]["length"])/2)), 8*(i-1), eval(data["line"+str(i)]["color"]))
					#debug infos
					print "Drawing Text: " + data["line"+str(i)]["text"] + ", " + str(int((width*float(data["position"]))-(6*int(data["line"+str(i)]["length"])/2))) + ", " + str(8*(i-1)) + ", " + str(eval(data["line"+str(i)]["color"]))
				updateLeds()
				
		elif data["type"] == "image": #show animation
			if data["extension"] == "gif":
				anim = image.ImageAnim(led, "/var/www/Image.gif")
				anim.run(fps = fps)
			else: #show static image
				image.showImage(led = led, imagePath = "/var/www/Image."+data["extension"], offset=(108,0))
				updateLeds()

updateDisplay()

#check for file change
wm = pyinotify.WatchManager()
mask = pyinotify.IN_MODIFY	# watched events
class EventHandler(pyinotify.ProcessEvent):
	global prevTime
	prevTime = time.time()	
	def process_IN_MODIFY(self, event):
		global prevTime
		if time.time() > prevTime + cooldown:
			prevTime = time.time()
			updateDisplay()
		
handler = EventHandler()
notifier = pyinotify.Notifier(wm, handler)
wdd = wm.add_watch(path, mask, rec=True)
notifier.loop()





