path = 'Text.txt'
width = 232
height = 16
triggers = 0

fps = 7


import json

from bibliopixel.led import *
from bibliopixel.animation import *
from bibliopixel.matrix_animations import *
from bibliopixel.strip_animations import *

import bibliopixel.image as image

#from bibliopixel.drivers.dummy_driver import DriverDummy
#driver = DriverDummy(width*height,500)

#from bibliopixel.drivers.i2c_driver import DriverI2C
#driver = DriverI2C(width*height, 42)

#from bibliopixel.drivers.serial_driver2 import DriverSerial
#driver = DriverSerial(type = 0, num = width*height, dev = "/dev/ttyAMA0", baud = 4000000, SPISpeed = 12, gamma = range(256))

from bibliopixel.drivers.visualizer import DriverVisualizer
driver = DriverVisualizer(width = width, height = height, pixelSize = 10)

led = LEDMatrix(driver, width, height, rotation = MatrixRotation.ROTATE_90, serpentine = False)



#led.fillScreen((1,1,1))
#led.drawText("Test",50,8,eval("colors.Green"))
#led.update()



def updateDisplay():
		led.all_off()
		
	#try: 
		f = open(path,'r')
		jsonData = f.read()
		if jsonData != "":
			print jsonData
			data = json.loads(jsonData)
			if data["type"] == "text":
				if data["line1"]["scrolling"] == "on":
					anim = ScrollText(led, data["line1"]["text"], width, 4, color = eval(data["line1"]["color"]))
					anim.run(fps = fps)
				else:
					#for i in range(1,int(data["lines"])+1): #	  <                           calculate middle of text                                 >
					#	led.drawText(data["line"+str(i)]["text"], int((width*float(data["position"]))-(6*int(data["line"+str(i)]["length"])/2)), 8*(i-1), eval(data["line"+str(i)]["color"]))
					#	print "Drawing Text: " + data["line"+str(i)]["text"] + ", " + str(int((width*float(data["position"]))-(6*int(data["line"+str(i)]["length"])/2))) + ", " + str(8*(i-1)) + ", " + str(eval(data["line"+str(i)]["color"]))
					led.drawText("test",100,4,colors.Green)
					print("Updating Display... ")
					led.update()
					print "done"
					
			elif data["type"] == "image":
				if data["extension"] == "gif":
					anim = image.ImageAnim(led, "/var/www/Image.gif")
					anim.run(fps = fps)
				else:
					image.showImage(led, "/var/www/Image."+data["extension"])
					print "Updating Display"
					led.update()
	#except:
	#	print("something went wrong at updating")

updateDisplay()





