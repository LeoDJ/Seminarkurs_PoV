from bibliopixel.drivers.visualizer import *
from bibliopixel.led import *
from bibliopixel.animation import *
from matrix_animations import *
from strip_animations import *

driver = DriverVisualizer(width = 96, height = 16, pixelSize = 5)

led = LEDMatrix(driver, rotation = MatrixRotation.ROTATE_0, vert_flip = False)

try:
	led.drawText(text = "test", color = colors.Green, size = 2)
	led.update()
	scroll = ScrollText(led, "Maniacal Labs Rules!", 96, 0, color = colors.Orange, size = 2)
	bounce = BounceText(led, "Maniacal Labs", 32, 8, color = colors.Orange, size =2)

	while True:
		 led.all_off()
		 scroll.run(fps = 20, untilComplete = True, max_cycles = 2)
		 led.all_off()
		 bounce.run(fps = 20, untilComplete = True, max_cycles = 2)

	print "done!"

except KeyboardInterrupt:
	pass

led.all_off()
led.update()