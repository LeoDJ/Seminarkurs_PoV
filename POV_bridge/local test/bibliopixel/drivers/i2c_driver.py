from driver_base import *


import smbus
#import ctypes
#import libbcm2835._bcm2835 as soc

#import quick2wire.i2c as i2c
import time
import RPi.GPIO as GPIO
GPIO.setmode(GPIO.BOARD)

bus = smbus.SMBus(1)
requestPin = 7

def try_io(call, tries=10):
    assert tries > 0
    error = None
    result = None

    while tries:
        try:
            result = call()
        except IOError as e:
            error = e
            tries -= 1
        else:
            break

    if not tries:
        raise error

    return result


def Interrupt(channel):
		print "Sending data... ",
		count = 0
		errCount = 0
		
		for b in ledData:
			#print b
			count +=1
			try:
				try_io(lambda: bus.write_byte(addr, b))
			except IOError:
				errCount += 1
			"""	
			try:
				bus.write_byte(addr, b)
			except IOError, err:
				#print("Fucking I/O Error, go away!!!")
				errCount += 1
			"""
		print "Data sent (" + str(count) + " bytes) and " + str(errCount) + " fucking I/O Errors!"
		done = True


"""def Interrupt(channel):
		soc.bcm2835_i2c_begin()
		arr = (ctypes.c_int * len(ledData))(*ledData)
		soc.bcm2835_i2c_write(pointer(arr), 3712)
		soc.bcm2835_i2c_end()
		print "Data sent" """	

"""def Interrupt(channel):		
		with i2c.I2CMaster() as bus:    
			bus.transaction(
				i2c.writing_bytes(address, ledData))		
		print "Data sent"		 """
		
def printData(pData):
	count0=0
	count1=0
	for b in pData:
		if b == 0:
			count0 += 1
		else:
			count1 += 1
	print "Buffer contents: <" + str(count0) + "> 0\'s and <" + str(count1) + "> others"
		

class DriverI2C(DriverBase):

	
	
	def __init__(self, num, address, timeout = 0):
		super(DriverI2C, self).__init__(num, address, timeout)
		self._timeout = timeout
		global addr
		addr = address
		#soc.bcm2835_init()
		#soc.bcm2835_i2c_setSlaveAddress(addr)
		GPIO.setup(requestPin, GPIO.IN, pull_up_down = GPIO.PUD_DOWN)
		GPIO.add_event_detect(requestPin, GPIO.RISING, callback = Interrupt)
	

	
	
	#Push new data to strand
	def update(self, data):
		#print data
		global done
		done = False
		global ledData
		ledData = data
		printData(data)
		Interrupt(0)
		while(not done):
			time.sleep(0.001) #sleep 1ms
