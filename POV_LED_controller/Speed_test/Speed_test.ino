unsigned long prevMicros=0, prevMillis=0;
unsigned long i;
void setup() {
  Serial.begin(115200);
  delay(2000);
  Serial.println(F_CPU);
  pinMode(22, INPUT_PULLUP);
}

void loop() {
  if(digitalRead(22))
  {
    i = 0;
    while(micros()-prevMicros < 1000000)
    {
      i++;
    }
    Serial.print(i);
    Serial.print(", ");
    Serial.println(micros());
    prevMicros = micros();
  }
  else
  {
    i = 0;
    while(millis()-prevMillis < 1000)
    {
      i++;
    }
    Serial.print(i);
    Serial.print(", ");
    Serial.println(millis());
    prevMillis = millis();
  }
}
