#include <Wire.h>
const double pi = 3.14159265359;

//                                      mm               mm
const double radius = 255, ledPitch = 6.85; 
const int ledCount = 16;
const int width = (int)((2*pi*radius)/ledPitch);
byte testData[256];
void setup() 
{
  Wire.begin(42);
  Wire.onRequest(requestEvent);
}

void loop() 
{
  
}

void requestEvent()
{
  digitalWrite(13, HIGH);
  genData();
  Wire.write(testData, sizeof(testData));
  digitalWrite(13, LOW);
}

void genData()
{
  for(int i = 0; i < sizeof(testData); i++)
  {
    testData[i] = i;
  }
}

