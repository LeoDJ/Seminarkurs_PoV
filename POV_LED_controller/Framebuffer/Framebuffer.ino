#include <SPI.h>
#include <Wire.h>
#include <FastLED.h> //only for RGB-datatype
#include <avr/io.h>
#include <avr/interrupt.h>

#define DEBUG 0


#define sensPin     15
#define requestPin  23
#define resetPin    21

const double radius = 255, ledPitch = 6.85; //mm
const char onLength = 7; //mm
const int ledCount = 16;
SPISettings spiSet(24000000, MSBFIRST, SPI_MODE0);
const int deviceID = 42;
const double pi = 3.14159265359;
const int width = (int)((2 * pi*radius) / ledPitch);
CRGB ledBuf[width][ledCount] = {0};
CRGB skPovText[34][ledCount] = { { {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 255}, {0, 0, 255}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0} }, { {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 255}, {0, 0, 0}, {0, 0, 0}, {0, 0, 255}, {0, 0, 0}, {0, 0, 0}, {0, 0, 255}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0} }, { {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 255}, {0, 0, 0}, {0, 0, 0}, {0, 0, 255}, {0, 0, 0}, {0, 0, 0}, {0, 0, 255}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0} }, { {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 255}, {0, 0, 0}, {0, 0, 0}, {0, 0, 255}, {0, 0, 0}, {0, 0, 0}, {0, 0, 255}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0} }, { {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 255}, {0, 0, 255}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0} }, { {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0} }, { {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 255}, {0, 0, 255}, {0, 0, 255}, {0, 0, 255}, {0, 0, 255}, {0, 0, 255}, {0, 0, 255}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0} }, { {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 255}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0} }, { {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 255}, {0, 0, 0}, {0, 0, 255}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0} }, { {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 255}, {0, 0, 255}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 255}, {0, 0, 255}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0} }, { {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0} }, { {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0} }, { {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0} }, { {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0} }, { {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0} }, { {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0} }, { {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0} }, { {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 255}, {0, 0, 255}, {0, 0, 255}, {0, 0, 255}, {0, 0, 255}, {0, 0, 255}, {0, 0, 255}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0} }, { {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 255}, {0, 0, 0}, {0, 0, 0}, {0, 0, 255}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0} }, { {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 255}, {0, 0, 0}, {0, 0, 0}, {0, 0, 255}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0} }, { {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 255}, {0, 0, 0}, {0, 0, 0}, {0, 0, 255}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0} }, { {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 255}, {0, 0, 255}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0} }, { {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0} }, { {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 255}, {0, 0, 255}, {0, 0, 255}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0} }, { {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 255}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 255}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0} }, { {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 255}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 255}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0} }, { {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 255}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 255}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0} }, { {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 255}, {0, 0, 255}, {0, 0, 255}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0} }, { {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0} }, { {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 255}, {0, 0, 255}, {0, 0, 255}, {0, 0, 255}, {0, 0, 255}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0} }, { {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 255}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0} }, { {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 255}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0} }, { {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 255}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0} }, { {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 255}, {0, 0, 255}, {0, 0, 255}, {0, 0, 255}, {0, 0, 255}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0} } };
byte spiBuf[(1 + ledCount + 4) * 4]; //1 Start-Frame, x LED-Frames, 4 End-Frames (for up to 256 LEDs)
unsigned long rotTime = 0, prevRotTime = 0, prevMicros = 0, curMicros, onDelay, curIndex, i2cIndex = 0;
bool firstStart = true;
int packetCounter = -1, cmd = -1, len = -1;

void setup() {
  // put your setup code here, to run once:
#if DEBUG == 1
  Serial.begin(115200);
  delay(2000);
  Serial.println(">> PoV Framebuffer <<");
#endif

  SPI.begin();
  //Serial1.begin(9600); //max. 6MHz
  Serial1.begin(1152000);
  /*Wire.begin(deviceID);
  Wire.onReceive(receiveEvent);*/


  pinMode(sensPin, INPUT);
  attachInterrupt(sensPin, sensTrig, FALLING);

  pinMode(requestPin, OUTPUT);

  pinMode(resetPin, INPUT_PULLUP);
  attachInterrupt(resetPin, resetSerial, FALLING);

  setupBufs();
  ledUpdate(0);


}

void loop() {
  povTest();

}

void serialEvent1() //SerialEvent occurs whenever a new data comes in the hardware serial RX.  This routine is run between each time loop() runs
{
  while (Serial1.available())
  {
    unsigned char c = Serial1.read();
    #if DEBUG
      if(packetCounter < 10)
        Serial.println((String)packetCounter + ": " + (String)(int)c);
    #endif
    switch (packetCounter)
    {
      case 0:
        if(c < 6)
        {
          cmd = c;
          Serial1.write(255);
        }
        else packetCounter = -1;
        break;
      case 1:
        len = c;
        break;
      case 2:
        len += (int)(c << 8);
      #if DEBUG
        Serial.println("length: " + (String)len);
      #endif
        break;
    }

    if (cmd == 2 && packetCounter > 2) //ledData
      rcvPixel(packetCounter - 3, c);

    if (packetCounter == len + 2)
    {
      #if DEBUG
        Serial.println((String)packetCounter+": all Data received!");
      #endif
      packetCounter = 0;
    }
    else
      packetCounter++;
  }
}

void rcvPixel(int index, byte data)
{
  int y = (index / 16) / 3;
  int x = (index % 48) / 3;
  byte l = index % 3;
  ledBuf[y][x][l] = data;
#if DEBUG == 2
  Serial.println((String)index + "=[" + (String)y + "][" + (String)x + "][" + (String)l + "]");
#endif

}

void povTest() //check if it's time for the next "frame"
{
  curMicros = micros();
  if (curMicros - prevMicros >= onDelay)
  {
    prevMicros = curMicros;
    povUpdate();
  }
}


void setupBufs()  //reset buffers to known state
{

  for (int i = 0; i < ledCount; i++)
  {
    ledBuf[1][i] = CRGB(63, 0, 0);
  }
  for (int i = 0; i < ledCount; i++)
  {
    ledBuf[2][i] = CRGB(0, 63, 0);
  }
  for (int i = 0; i < ledCount; i++)
  {
    ledBuf[3][i] = CRGB(0, 0, 63);
  }


  for (int i = 0; i < 34; i++) //"SK PoV" Text
  {
    for (int j = 0; j < ledCount; j++)
    {
      ledBuf[100 + i][j] = skPovText[i][j];
    }
  }

  ledConvData(0);
}

void resetSerial() //init new transfer
{
  packetCounter = 0;
  #if DEBUG
    Serial.println("New Data... Resetting");
  #endif
}

void povUpdate()
{
  if (rotTime != 0)
  {
    curIndex = (curMicros - prevRotTime) / onDelay;
    if (curIndex < width)
    {
    #if DEBUG == 1
      Serial.print("\n\nIndex: " + (String)curIndex + "");
	#endif
      ledUpdate(curIndex); //update the current column
    }
    else curIndex = width; //prevent overflow
  }

}

void povCalcTimings() //runs only once per revolution
{
  rotTime = micros() - prevRotTime;
  prevRotTime = micros();
  onDelay = (unsigned long)(onLength * rotTime) / (2 * pi * radius);
#if DEBUG == 1
  Serial.println("\ncalculated timings: delay=" + (String)onDelay + "us, rotTime=" + (String)rotTime + "us");
#endif
}

void sensTrig() //gets executed, when sensor is at home position
{
  cli();
  digitalWrite(requestPin, HIGH);
  i2cIndex = 0;
  #if DEBUG == 1
    Serial.print("\nSensor triggered");
  #endif
  povCalcTimings();
  //requestNewData();
  digitalWrite(requestPin, LOW);
  sei();
}



void ledUpdate(int index) //function to call to show a coloumn out of the frame-buffer
{
  ledConvData(index);
  ledSendData();
}

void ledConvData(int index) //convert the given column of the frame-buffer to the actual SPI-data that gets sent out
{
  //Convert the RGB buffer to the actual bits that will be sent out over SPI

#if DEBUG == 1
  Serial.print("\nLED RBG data: ");
#endif
  for (int i = 1; i <= ledCount; i++) //i=1 skip Start-Frame
  {
    spiBuf[i * 4 + 0] = 0xFF;
    spiBuf[i * 4 + 1] = ledBuf[index][i - 1].b;
    spiBuf[i * 4 + 2] = ledBuf[index][i - 1].g;
    spiBuf[i * 4 + 3] = ledBuf[index][i - 1].r;
#if DEBUG == 1
    Serial.print(ledBuf[index][i - 1].r);
    Serial.print(", ");
    Serial.print(ledBuf[index][i - 1].g);
    Serial.print(", ");
    Serial.print(ledBuf[index][i - 1].b);
    Serial.print(";  ");
#endif
  }
  //no need to set the start/end frame, because spiBuf is initialized with zeros
}


void ledSendData() //send out the SPI-data to the LEDs
{
#if DEBUG == 1
  Serial.print("\nraw SPI Data: ");
#endif
  SPI.beginTransaction(spiSet);
  for (int i = 0; i < sizeof(spiBuf); i++)
  {
    SPI.transfer(spiBuf[i]);
#if DEBUG == 1
    Serial.print(spiBuf[i]);
    Serial.print(", ");
#endif
  }

  SPI.endTransaction();
}
