#include <FastLED.h>
#include <Wire.h>
const double pi = 3.14159265359;

//                                      mm               mm
const double radius = 255, ledPitch = 6.85; 
const int ledCount = 16;
const int width = (int)((2*pi*radius)/ledPitch);
CRGB ledBuf[width][ledCount];
const int deviceID = 42;

void setup()
{
  Wire.begin();
}
  
void loop()
{
  Wire.requestFrom(deviceID, sizeof(ledBuf));
  
  if(Wire.available())
  {
    Serial.print("Received ");
    Serial.print(Wire.available());
    Serial.print(" bytes: ");
    while(Wire.available())
    {
      
      for(int w = 0; w < 16; w++) //limit to 16 due to small RAM on PiSim
      {
        for(int l = 0; l < ledCount; l++)
        {
          ledBuf[w][l] = Wire.read();
          Serial.print(ledBuf[w][l]);
          Serial.print(" ,");
        }
      }
    }
    Serial.println("");
  }
  delay(5000);
}
