#include <SPI.h>
#include "FastLED.h"
#define NUM_LEDS 500

CRGB leds[NUM_LEDS];

unsigned long prevMicros=0;

void setup() {
  // put your setup code here, to run once:
  delay(500);
  FastLED.addLeds<APA102, BGR>(leds, NUM_LEDS);
}

void loop() {
  // put your main code here, to run repeatedly:
  prevMicros=micros();
  /*for(int i = 0; i < 128*10; i++)
  {
    leds[i/10]=CRGB::Black;
    leds[i/10+1]=CRGB(31,15,0);
    FastLED.show();
  }*/
  
  for(int i = 0; i < 220*8; i++)
  {
    FastLED.showColor(CRGB::Black);
  }
  Serial.println(micros()-prevMicros);
}
