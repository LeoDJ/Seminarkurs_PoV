#include <Servo.h>
#include <SPI.h>
#include <nRF24L01.h>
#include <RF24.h>

#define DEBUG 0

//Konstantendeklaration

#define CE_PIN    9 
#define CSN_PIN   10
#define ledPin    5	//Empfangs-LED
#define errLedPin 4	//Überhitzungs-LED
#define escPin    6	//Motorregler Anschluss
#define tempPin   A0//LM35 Temperatur Sensor

#define maxTemp  40	//Maximaltemperatur in °C
#define timeout  100
#define idleVal  92	//Aus-Wert des Motorreglers
#define minVal   90
#define maxVal   180

//Variablendeklaration

const uint64_t pipe = 0xE8E8F0F0E5LL;	//Funkkanal festlegen

int val;
unsigned long lost, prevMillis;

RF24 radio(CE_PIN, CSN_PIN); //Funk-Objekt erstellen

Servo esc;

void setup()	//init()
{
  pinMode(ledPin, OUTPUT);
  pinMode(errLedPin, OUTPUT);
  #if DEBUG					//Schließt Code vom Komilieren aus, wenn DEBUG=0
	Serial.begin(115200); 	//Serielle Debug-Konsole nur verfügbar, wenn DEBUG=1
  #endif
  esc.attach(escPin);
  
  radio.begin();			//Funkmodul initialisieren
  radio.openReadingPipe(1,pipe);
  radio.startListening();
  delay(1000);
  #if DEBUG
	Serial.println(">> NRF24L01 Motor Control Receiver <<");
  #endif
  prevMillis = 0;
}


void loop()	//main()
{
  lost = millis() - prevMillis;
  
  if (radio.available())			//wenn Funksignal verfügbar
  {
    prevMillis = millis();
    radio.read(&val, sizeof(int));	//Paket auslesen
    writeESC();						//Regler ansteuern
  }

  else 									//wenn kein Funksignal
  {    
      digitalWrite(ledPin, LOW);	
      if (lost < timeout) writeESC();   //Wenn Signal kürzer als 0,1s verloren wurde, Motor weiter betreiben (um kleinere Verbindungsfehler zu kompensieren)
      else esc.write(idleVal);          //Ansonsten Motor ausschalten
      
  }
  delay(5);
}

void writeESC()							//Regler ansteuern
{
  if(readTemp() < maxTemp)  			//Überhitzungsschutz
  {
    if(minVal < val && val < maxVal)	//wenn empfangener Wert innerhalb der Grenzen
    {
	  #if DEBUG
		Serial.println(val);
	  #endif
      esc.write(val);					//Geschwindigkeits-Wert an Regler schicken
      digitalWrite(ledPin, HIGH);		//Status LEDs ansteuern
      digitalWrite(errLedPin, LOW);
    }
    else								//wenn Wert außerhalb der Grenzen
    {
	  #if DEBUG
		Serial.print(val);
		Serial.println(" - OUT OF RANGE (Check Receiver)");
	  #endif
      esc.write(idleVal);				//Motor ausschalten
      digitalWrite(errLedPin, HIGH);	//Rote LED anschalten
    }
  }
  else 								//Wenn Temperatur > max
  {
	#if DEBUG
		Serial.println("OVERTEMP");
	#endif
    esc.write(idleVal);				//Motor ausschalten
    digitalWrite(errLedPin, HIGH);	//Rote LED anschalten
  } 
}



float readTemp()								//Temperatur auslesen und umwandeln
{
  float temp = analogRead(tempPin)*500.0/1024;	//5*1024 = Analoge Spannung; 10mV/°C -> *100
  #if DEBUG
	Serial.print(temp);
	Serial.print(" C     ");
  #endif
  return temp;									//Wert zurückgeben
}

