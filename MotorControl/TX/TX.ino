#include <SPI.h>
#include <nRF24L01.h>
#include <RF24.h>

#define DEBUG 0

//Konstantendeklaration

#define CE_PIN   9
#define CSN_PIN 10
#define potPin  A0

//Variablendeklaration

const uint64_t pipe = 0xE8E8F0F0E5LL; //Funkkanal festlegen

RF24 radio(CE_PIN, CSN_PIN); //Funk-Objekt erstellen
int pot; 

void setup()  //init()
{
  #if DEBUG	//Schließt Code vom Komilieren aus, wenn DEBUG=0
	Serial.begin(115200); //Serielle Debug-Konsole nur verfügbar, wenn DEBUG=1
	Serial.println(">> NRF24L01 Motor Control Transmitter <<");
  #endif
  radio.begin();	//Funkmodul initialisieren
  radio.openWritingPipe(pipe);
}


void loop()  //main()
{
  //Potentiometerwert einlesen und direkt umwandeln
  pot = map(analogRead(potPin),0,1023,92,150);
  #if DEBUG 
	Serial.println(pot);
  #endif
  //Wert senden
  radio.write(&pot, sizeof(int));
}
