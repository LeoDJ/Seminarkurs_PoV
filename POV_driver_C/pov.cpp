#include "pov.h"


POV::POV()
{
	prevTime = 0;
	prevLedTime = 0;
	start = false;
	led led; //quickFix
	width = led.giveWidth();
	for (int i = 0; i < width; i++)
	{
		for (int j = 0; j < 16; j++)
		{
			led.setBuf(i,j,(RGB){0,0,0});
		}
	}
	for (int i = 0; i < width; i++)
	{
		/*if(i%2==0)*/	led.setBuf(i,0,(RGB){128,128,128});
		if(i%2==0)	led.setBuf(i,14,(RGB){0,255,0});
	}
	for(int j = 0; j < 5; j++)
	{
		int data[5] = {0x7e,0x88,0x88,0x88,0x7e};
		uint8_t mask = 1;
		for (mask = 1; mask > 0; mask <<= 1)
		{
			int idx = (int)log2(mask);
			if(mask & data[j]) led.setBuf(j+70,idx,(RGB){255,255,255});
		}
	}
	for (int i = 80; i < 84; i++)
	{
		for (int j = 0; j < 16; j++)
		{
			led.setBuf(i,j,(RGB){0,0,255});
		}
	}
	for (int i = 84; i < 88; i++)
	{
		for (int j = 0; j < 16; j++)
		{
			led.setBuf(i,j,(RGB){0,255,0});
		}
	}
	for (int i = 88; i < 92; i++)
	{
		for (int j = 0; j < 16; j++)
		{
			led.setBuf(i,j,(RGB){255,0,0});
		}
	}
}

void POV::checkRotation()  //Timing using constant time poll
{
	led led;	//quickFix
	if(bcm2835_gpio_eds(RPI_GPIO_P1_22))		//check for falling edge flag
	{
		start = true;
		//printf("%f\n",getTime());
		calcTimings();
		led.reset(); //[TODO] return to first col / get next frame
		bcm2835_gpio_set_eds(RPI_GPIO_P1_22);	//clear edge detect flag
	}
	else
	{
		curTime = getTime();
		if(curTime - prevLedTime > delay && start)//timing check
		{
			//led.next(); //[TODO] get next col
			int i = (curTime-prevTime) / delay;
			if(i < width-1) led.update(i); //when inside bounds -> update
			else led.clearLeds();
			//printf("%f\n",getTime());
			prevLedTime = getTime();
		}
	}
}

double POV::getTime()
{
	clock_gettime(CLOCK_REALTIME, &clock);
	time = (double)clock.tv_sec + ((double)clock.tv_nsec / 1000000000);
	return time;
}

void POV::calcTimings()
{
	if(prevTime != 0)
	{
		revTime = getTime() - prevTime;
		//         pixel count	
		delay = revTime / ((2*M_PI*radius)/pitch);
		//printf("pixelTime: %f\n",delay);
		printf("RPS: %f\n",1/revTime);
	}
	prevTime=getTime();
}