#include <stdio.h>
#include <math.h>
#include <bcm2835.h>


#define LED_num 16

#define radius	225 //mm
#define pitch	6.85 //mm

struct RGB
{
	uint8_t R;
	uint8_t G;
	uint8_t B;
};

class led
{
	public:
		void setLED(int, RGB);
		void printBuffer();
		void convertData();
		void sendData();
		void printData();
		void update(int);
		void clearLeds();
		led();
		void reset();
		void next();
		void setBuf(int, int, RGB);
		int giveWidth();
	private:
		RGB buf[LED_num];
		uint8_t ledData[(LED_num*4)  +4  +LED_num/4]; //Actual LED buffer, +4 for the start frame and +16/4 for the end frame (so all LEDs receive their update)
		//gnublin_spi spi;
		int tmpIndex, width;
		RGB buf2d[(int)((2*M_PI*radius)/pitch)][LED_num];

};