#include <time.h>
#include "led.h"



class POV
{
	public:
		void checkRotation();
		POV();
		
	private:
		struct timespec clock;
		double time, delay, curTime, prevTime, prevLedTime, revTime;
		double getTime();
		void calcTimings();
		bool start;
		int width;
		
		

};