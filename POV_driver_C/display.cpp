#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string>
#include <sstream>
#include <iostream>
#include <math.h>
#include <time.h>
//#define BOARD RASPBERRY_PI
//#include "gnublin.h"
//#include <wiringPiSPI.h>
#include <bcm2835.h>

#include "led.h"
#include "pov.h"


/*template < typename T > std::string to_string( const T& n )
{
    std::ostringstream stm ;
    stm << n ;
    return stm.str() ;
}*/



/*class colors
{
	public:
		RGB red = (RGB){255,0,0};
		RGB green = (RGB){0,255,0};
		RGB blue = (RGB){0,0,255};
};*/





void init()
{
	bcm2835_init();
	bcm2835_gpio_fsel(RPI_GPIO_P1_22, BCM2835_GPIO_FSEL_INPT); //Declare GPIO 25 (Pin 22) as Input
	bcm2835_gpio_aren(RPI_GPIO_P1_22); //enable falling edge detection
	
}

int main()
{
	init();
	POV pov;
	
	while(1)
	{
		pov.checkRotation();
	}
	
	/*printf(">> POV driver test <<\n");
	led APA102;
	APA102.setLED(0,(RGB){63,0,0});
	RGB tmp = {0,63,0};
	APA102.setLED(1,tmp);
	APA102.setLED(2,(RGB){0,0,63});
	APA102.setLED(14,(RGB){63,0,63});
	APA102.setLED(8,(RGB){255,255,255});
	for(int i = 0; i<16; i++) APA102.setLED(i,(RGB){31,31,31});
	APA102.printBuffer();
	APA102.convertData();
	APA102.sendData();
	APA102.printData();*/

	bcm2835_spi_end();
    bcm2835_close();
}