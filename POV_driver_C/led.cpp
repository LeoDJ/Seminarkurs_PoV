#include "led.h"

led::led()
{
	width = sizeof(buf2d)/48;
	for (int i=0; i < LED_num; i++) //reset buf
	{
		buf[i] = (RGB){0,0,0};
	}
	for (int i = 0; i < width; i++)
	{
		for (int j = 0; i < LED_num; i++)
		{
			buf2d[i][j] = (RGB){0,0,0};
		}
	}
	for (int i = 0; i < sizeof(ledData); i++)
	{
		ledData[i] = 0;
	}

	//Init SPI Interface
	bcm2835_spi_begin();
	bcm2835_spi_setClockDivider(BCM2835_SPI_CLOCK_DIVIDER_128); //~2MHz
	bcm2835_spi_setDataMode(BCM2835_SPI_MODE0);
	bcm2835_spi_chipSelect(BCM2835_SPI_CS0);


	//spi.setSpeed(10000); //Start SPI with 16MHz
	//spi.setCS(14);
}
int led::giveWidth()
{
	return width;
}
void led::setLED(int pos, RGB color)
{
	buf[pos] = color;
}

void led::printBuffer()
{
	for(int i=0; i < LED_num; i++)
	{
		printf("%u, %u, %u\n",buf[i].R, buf[i].G, buf[i].B);
	}
}

void led::convertData()
{
	//Convert the RGB buffer to the actual bits that will be sent out over SPI
	for (int i=1; i < LED_num; i++) //start at 4 (1*4) to start after the start frame
	{
		ledData[i*4 + 0] = 255;
		ledData[i*4 + 1] = buf[i-1].R; //i-1 because of the offset
		ledData[i*4 + 2] = buf[i-1].G; //RGB order is inverted on APA102
		ledData[i*4 + 3] = buf[i-1].B;
	}
	//no need to set the start/end frame, because ledData is initialized with zeros
}

/*void led::sendData()
{
	unsigned char spiData[sizeof(ledData)];
	for(int i = 0; i<sizeof(ledData);i++) spiData[i] = ledData[i];
	spi.send(spiData, sizeof(spiData));
}*/
void led::sendData()
{
	char spiData[sizeof(ledData)];
	for(int i = 0; i<sizeof(ledData);i++) spiData[i] = ledData[i];
	bcm2835_spi_writenb(spiData, sizeof(spiData));
}

void led::printData()
{
	for (int i=0; i < sizeof(ledData); i++)
	{
		printf("%u, ",ledData[i]);
	}
	printf("\n");
}

void led::update(int index)
{
	for (int i = 0; i < LED_num; i++)
	{
		buf[i] = buf2d[index][i];
	}
	convertData();
	sendData();
}

void led::clearLeds()
{
	for (int i=0; i < LED_num; i++)
	{
		buf[i] = (RGB){0,0,0};
	}
	convertData();
	sendData();
}

void led::setBuf(int x, int y, RGB val)
{
	buf2d[x][y] = val;
}

void led::next()
{
	if(tmpIndex < width) tmpIndex++;
	update(tmpIndex);
}

void led::reset()
{
	update(0);
}